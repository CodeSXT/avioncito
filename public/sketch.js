let size = 20
let radius = 180
let colors
let airplane = 0
let remaining = 0
let lastColor = null

function setup() {
  createCanvas(windowWidth, windowHeight);
  angleMode(DEGREES);
  translate(width/2, height/2);
  colors = [
    color(255, 0, 0),
    color(0, 0, 255),
    color(255, 255, 255)
  ]
}

function drawRoulette() {
  let c = 0
  for (let i = size; i <= 360; i+=size) {
    let x = cos(i) * radius;
    let y = sin(i) * radius;
    let px = cos(i - size) * radius;
    let py = sin(i - size) * radius;
    beginShape()
    fill(colors[c])
    vertex(0, 0)
    vertex(px, py)
    vertex(x, y)
    endShape()
    c += 1
    c = c % 3
  }
}

function drawAirplane() {
  let x = cos(airplane) * (radius - 20)
  let y = sin(airplane) * (radius - 20)
  circle(x, y, 10)
}

function draw() {
  if (lastColor != null) {
    background(lastColor)
  }
  translate(width/2, height/2);
  // background(220);
  drawRoulette()
  drawAirplane()
  if (remaining > 0) {
    airplane += 1
    airplane = airplane % 360
    remaining -= 1
    if (remaining == 0) {
      let x = cos(airplane) * (radius - 50)
      let y = sin(airplane) * (radius - 50)
      c = get(width/2 + x, height/2 + y)
      lastColor = color(c[0], c[1], c[2])
      // console.log(lastColor)
    }
  }
}

function mouseClicked() {
  remaining = int(random(10 * 30, 12 * 30))
  print(remaining)
}
